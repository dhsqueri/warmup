from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^users/login', 'warmuplogin.views.login'),
    url(r'^users/add', 'warmuplogin.views.add'),
    url(r'^TESTAPI/resetFixture', 'warmuplogin.views.reset'),
    url(r'^TESTAPI/unitTests', 'warmuplogin.views.unitTests'),
    url(r'^', 'warmuplogin.views.main'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
