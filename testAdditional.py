"""
Each file that starts with test... in this directory is scanned for subclasses of unittest.TestCase or testLib.RestTestCase
"""

import unittest
import os
import testLib

class TestUnit(testLib.RestTestCase):
    """Issue a REST API request to run the unit tests, and analyze the result"""
    def testUnit(self):
        respData = self.makeRequest("/TESTAPI/unitTests", method="POST")
        self.assertTrue('output' in respData)
        print ("Unit tests output:\n"+
               "\n***** ".join(respData['output'].split("\n")))
        self.assertTrue('totalTests' in respData)
        print "***** Reported "+str(respData['totalTests'])+" unit tests"
        # When we test the actual project, we require at least 10 unit tests
        minimumTests = 10
        if "SAMPLE_APP" in os.environ:
            minimumTests = 4
        self.assertTrue(respData['totalTests'] >= minimumTests,
                        "at least "+str(minimumTests)+" unit tests. Found only "+str(respData['totalTests'])+". use SAMPLE_APP=1 if this is the sample app")
        self.assertEquals(0, respData['nrFailed'])


        
class TestAddUser(testLib.RestTestCase):
    """Test adding users"""
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        """
        Check that the response data dictionary matches the expected values
        """
        expected = { 'errCode' : errCode }
        if count is not None:
            expected['count']  = count
        self.assertDictEqual(expected, respData)

    def testAdd1(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : 'password'} )
        self.assertResponse(respData, count = 1)

    def testAdd2(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 1)
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user2', 'password' : 'password2'})
        self.assertResponse(respData, count = 1)


class TestUserLogin(testLib.RestTestCase):
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        """
        Check that the response data dictionary matches the expected values
        """
        expected = { 'errCode' : errCode }
        if count is not None:
            expected['count']  = count
        self.assertDictEqual(expected, respData)

    def testLogin(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 1)
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 2)

    def testMultipleLogin(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 1)
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 2)
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 3)

    def testWrongPassword(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 1)
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'user1', 'password' : 'password2'})
        self.assertResponse(respData, count = None, errCode = -1)

    def testNoUser(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 1)
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'user2', 'password' : 'password1'})
        self.assertResponse(respData, count = None, errCode = -1)


class TestSameUser(testLib.RestTestCase):
    """Test adding users"""
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        """
        Check that the response data dictionary matches the expected values
        """
        expected = { 'errCode' : errCode }
        if count is not None:
            expected['count']  = count
        self.assertDictEqual(expected, respData)

    def testAddSameUser(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 1, errCode = 1)
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = None, errCode = -2)


class TestNoUser(testLib.RestTestCase):
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        expected = { 'errCode' : errCode }
        if count is not None:
            expected['count'] = count
        self.assertDictEqual(expected, respData)

    def testNoUserAdded(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user1', 'password' : 'password1'})
        self.assertResponse(respData, count = 1, errCode = 1)
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'user2', 'password' : 'password2'})
        self.assertResponse(respData, count = None, errCode = -1)


class TestBadUser(testLib.RestTestCase):
    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        expected = { 'errCode' : errCode }
        if count is not None:
            expected['count'] = count
        self.assertDictEqual(expected, respData)

    def testLongUser(self):
        username = 'a'
        for x in range(1,150):
            username = username + 'a'
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : username, 'password' : 'password1'})
        self.assertResponse(respData, count = None, errCode = -3)

    def testLongPassword(self):
        password = 'a'
        for x in range(1,150):
            password = password + 'a'
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'user1', 'password' : password})
        self.assertResponse(respData, count = None, errCode = -4)

