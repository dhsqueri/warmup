from django.db import models

SUCCESS               =   1  # : a success
ERR_BAD_CREDENTIALS   =  -1  # : (for login only) cannot find the user/password pair in the data$
ERR_USER_EXISTS       =  -2  # : (for add only) trying to add a user that already exists
ERR_BAD_USERNAME      =  -3  # : (for add, or login) invalid user name (only empty string is inv$
ERR_BAD_PASSWORD      =  -4

#Model that stores the user information in the database
class UserModel(models.Model):

	username = models.CharField(max_length=128)
	password = models.CharField(max_length=128)
	logins = models.PositiveSmallIntegerField(default=1) #initially 1 when user first created

	def __unicode__(self):
		return self.username



#Will log in a user if they exist in the database, otherwise returns error code
def userLogin(user, passwrd):
        tmp_list = UserModel.objects.filter(username=user)

        if not tmp_list:
		return -1
        if tmp_list[0].password != passwrd:
                return -1
        else:   
		tmp_user = tmp_list[0]
		tmp_user.logins = tmp_user.logins + 1
                tmp_user.save()
                return tmp_user.logins

#Adds user to the database, unless it violates one of the rules
def addUser(user, passwrd):
	tmp_list = UserModel.objects.filter(username=user)
	if not not tmp_list: #User already exists
		return -2
	if not user: #Empty user name
		return -3
	if len(user) > 128: #User name is too long
		return -3
	if len(passwrd) > 128: #Password is too long
		return -4
	else:
		tmp_user = UserModel(username=user, password=passwrd)
		tmp_user.save()
		return 1

#Resets the database to empty
def TESTAPI_resetFixture():
	for user in UserModel.objects.all():
		user.delete()
	return 1
