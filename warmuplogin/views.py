from django.http import HttpResponse
from django.shortcuts import render_to_response
from warmuplogin.models import *
from warmuplogin.unitTester import *
from django.views.decorators.csrf import csrf_exempt
import json

#Main homepage
@csrf_exempt
def main(request):
        if request.method == 'GET':
                return render_to_response('warmup.html')
        elif request.method =='POST':
                return HttpResponse('<h1>Did not get the right url</h1>')


@csrf_exempt
def login(request):
	if request.method == 'GET':
		return HttpResponse('<h1>Login Page was Found</h1.')  #Testing purposes
	elif request.method == 'POST':
		decoded = json.loads(request.body) #decode JSON dictionary
		username = decoded['user'] #first key is username string
		password = decoded['password'] #second key is password string
	
		result = userLogin(username, password)
		if result > 0:
                        encoded = json.dumps({'errCode' : SUCCESS, 'count' : result})
                        response = HttpResponse(encoded, content_type='application/json')
                        return response
		else:
                        encoded = json.dumps({'errCode' : result})
                        response = HttpResponse(encoded, content_type='application/json')
                        return response

@csrf_exempt
def add(request):
	if request.method == 'GET':
		return HttpResponse('<h1>Add Page was Found</h1.')  #Testing purposes
	elif request.method =='POST':
		decoded = json.loads(request.body) #decode JSON dictionary
		username = decoded['user'] #first key is username string 
        	password = decoded['password'] #second key is password string
		result = addUser(username, password)
		if result > 0:
			encoded = json.dumps({'errCode' : result, 'count' : result})
                        response = HttpResponse(encoded, content_type='application/json')
			return response
                else:   
                        encoded = json.dumps({'errCode' : result})
			response = HttpResponse(encoded, content_type='application/json')
                        return response

@csrf_exempt
def reset(request):
	if request.method == 'GET':
		return HttpResponse('<h1>Reset Page was Found Again</h1>') #Testing purposes
	elif request.method == 'POST':
		result = TESTAPI_resetFixture()
		encoded = json.dumps({'errCode' : result})
		response = HttpResponse(encoded, content_type="application/json")
		return response


@csrf_exempt
def unitTests(request):
	if request.method == 'GET':
		return HttpResponse('<h1>Unit Test Page was Found</h1>') #Testing purposes
	elif request.method == 'POST':
		unitTestResults = runTests()
		encoded = json.dumps({'totalTests' : unitTestResults['total'], 'nrFailed' : unitTestResults['failed'], 'output' : 'tests completed'})
		response = HttpResponse(encoded, content_type="application/json")
		return response
