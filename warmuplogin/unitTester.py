from warmuplogin.models import *


def reset():
	TESTAPI_resetFixture()

def addTest1():
	reset()
	result = addUser("user1", "password")
	if result > 0:
		return 0 #pass
	else:
		print "addTest1 fail"
		return 1 #fail

def addTest2():
	reset()
	result1 = addUser("user1", "password1")
	result2 = addUser("user2", "password2")
	if result1 == SUCCESS and result2 == SUCCESS:
		return 0 #pass
	else:
		print "addTest2 fail"
		return1 #fail

def addExisting():
	reset()
	result1 = addUser("user1", "password")
	result2 = addUser("user1", "password")
	if result1 == SUCCESS and result2 == ERR_USER_EXISTS:
		return 0 #pass
	else:
		print "result1: " + str(result1)
		print "result2: " + str(result2)
		return 1 #fail

def addEmptyUser():
	reset()
	result = addUser( '', 'password')
	if result  == ERR_BAD_USERNAME:
		return 0 #pass
	else:
		print "addEmptyUser fail"
		return 1 #fail

def addLongUser():
	reset()
	username = ''
	for x in range(1,150):
		username = username + 'a'
	result = addUser(username, "password")
	if result == ERR_BAD_USERNAME:
		return 0 #pass
	else:
		print "Long username fail"
		return 1 #fail

def addLongPassword():
	reset()
	password = ''
	for x in range(1,150):
		password = password + 'a'
	result = addUser("user1", password)
	if result == ERR_BAD_PASSWORD:
		return 0 #pass
	else:
		print "Long password fail"
		return 1 #fail

def testReset():
	reset()
	result1 = addUser("user", "password")
	reset()
	result2 = addUser("user", "password")
	if result1 == SUCCESS and result2 == SUCCESS:
		return 0 #pass
	else:
		print "Reset test fail"
		return 1 #fail

def testLogin1():
	reset()
	result1 = addUser("user1", "password")
	result2 = userLogin("user1", "password")
	if result1 == SUCCESS and result2 >= SUCCESS:
		return 0 #pass
	else:
		print "result1: " + str(result1)
		print "result2: " + str(result2)
		return 1 #fail

def testLogin2():
	reset()
	result1 = userLogin("user", "password")
	if result1 == ERR_BAD_CREDENTIALS:
		return 0 #pass
	else:
		print "testLogin2 fail"
		return 1 #fail

def wrongPassword():
	reset()
	result1 = addUser("user", "password")
	result2 = userLogin("user", "pass")
	if result1 == SUCCESS and result2 == ERR_BAD_CREDENTIALS:
		return 0 #pass
	else:
		print "wrongPassword fail"
		return 1 #fail


def runTests():
	Number_of_Tests = 10

	results = addTest1() + addTest2() + addExisting() + addEmptyUser() + addLongUser()
	results = results + addLongPassword() + testReset() + testLogin1() + testLogin2() + wrongPassword()

	Tests_Failed = results
	return {'total' : Number_of_Tests, 'failed' : Tests_Failed}
